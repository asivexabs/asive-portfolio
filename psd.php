<link rel="stylesheet" type="text/css" href="gallery.css">
<link rel="stylesheet" type="text/css" href="nav.css">
<link rel="stylesheet" type="text/css" href="footer.css">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:200,400">
 <header></header>
  <nav>
    <a href="#" id="menu__icon"></a>
  <ul>
    <a href="index.php"><li>Home</li></a>
    <a href="about.php"><li>About</li></a>
    <a href="gallery.php"><li>Gallery</li></a>
    <a href="contact.php"><li>Contact</li></a>
  </ul>
  </nav>
<section>
  <h1>Featured Work</h1>
  
<ul class="photo-stack-grid">
  <li>
    <a href="#d">
      <figure class="photo-stack">
          
          
        <!-- 
            ***change aspect ratio of image - /200/400*** 
        -->
        <img src="img/about.png" />
          
          
      </figure>
      <span>Design</span>
    </a>
  </li>
  <li>
    <a href="#d">
      <figure class="photo-stack">
        <img src="img/carwash.png" />
      </figure>
      <span>Carwash</span>
    </a>
  </li>
  <li>
    <a href="#d">
      <figure class="photo-stack">
        <img src="img/contact.png" />
      </figure>
      <span>Contact Page</span>
    </a>
  </li>
  <li>
    <a href="#d">
      <figure class="photo-stack">
        <img src="img/Untitled-17.jpg" />
      </figure>
      <span>Messenger Platform</span>
    </a>
  </li>
  <li>
    <a href="#d">
      <figure class="photo-stack">
        <img src="img/homepage.jpg" />
      </figure>
      <span>Home Page Design</span>
    </a>
  </li>
  <li>
    <a href="#d">
      <figure class="photo-stack">
        <img src="img/sign-up form.jpg" />
      </figure>
      <span>Sign-up Form</span>
    </a>
  </li>
  <li>
     <a href="#d">
      <figure class="photo-stack">
        <img src="img/my-world.jpg"/>
      </figure>
      <span>Education & Work Experience</span>
   </a>
  </li>
  <li>
    <a href="#d">
      <figure class="photo-stack">
        <img src="img/gallery.jpg" />
      </figure>
      <span>Gallery Design</span>
      </li>
      </a>
  
</ul>

<a href="gallery.php"><button>view photoshop gallery</button></a>
<a href="explained.php"><button>explained projects</button></a>
</section>
<footer>
<img src="img/facebook.png" width="40" height="40">
<img src="img/google.png" width="40" height="40">
<img src="img/instagram.png" width="40" height="40">
<img src="img/linkedin.png" width="40" height="40">
<img src="img/twitter.png" width="40" height="40">
<img src="img/youtube.png" width="40" height="40">
<h6>Powered by www.mirumagency.com | ©2017</h6>
</footer>