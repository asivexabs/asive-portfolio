
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="testing.css">
</head>
<body>
 <!-- Begin Webpage -->
 <div class="webpage">
   <div class="red-brand-box">
     
     <div class="white-brand-box"></div>
   </div>
   
   <h3 class="presents-text">Square Labs presents</h3>
   
   <h1 class="title">Developer <br/> Course</h1>
   
   <button class="cta-button">
   Register today
   </button>
   
   <div class="right-triangle">
   </div>
   
 </div>
  

</body>
</html>