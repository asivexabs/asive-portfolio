<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="explained.css">
  <link rel="stylesheet" type="text/css" href="footer.css">
  <link href="https://fonts.googleapis.com/css?family=Bad+Script" rel="stylesheet">
</head>
<body>
<div class="jumbotron text-center">
  <div class="container">
    
    <h1>Explained Project</h1>
    <p style="color:#8ab300;">click the buttons to see the explanation of the project!</p>
    
  </div>
</div>
<div class="container"> 
  
  <div class="row">
    <div class="col-md-12 text-center">
      <h2 class="mrng-60-top">codepen <span class="highlight">& psd's</span></h2>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-3 col-sm-3 col-xs-6"> <a href="capitec.php" class="btn btn-sm animated-button sandy-one">Capitec</a> </div>
    <div class="col-md-3 col-sm-3 col-xs-6"> <a href="disney.php" class="btn btn-sm animated-button sandy-two">Disney</a> </div>
    <div class="col-md-3 col-sm-3 col-xs-6"> <a href="savanna.php" class="btn btn-sm animated-button sandy-three">Savanna</a> </div>
    <div class="col-md-3 col-sm-3 col-xs-6"> <a href="my-world.php" class="btn btn-sm animated-button sandy-four">isotope - filtering & sorting</a> </div>
  </div>
  <div class="col-md-3 col-sm-3 col-xs-6"> <a href="klipdrift.php" class="btn btn-sm animated-button sandy-three">Klipdrift</a> </div>
    
  
  
 <!--  
  <div class="row">
    <div class="col-md-12 text-center">
      <h2 class="mrng-60-top">psd</h2>
    </div>
  </div> -->
  
  <!-- <div class="row"> -->
    <div class="col-md-3 col-sm-3 col-xs-6"> <a href="sign_up.php" class="btn btn-sm animated-button gibson-two">sign up</a> </div>
    <div class="col-md-3 col-sm-3 col-xs-6"> <a href="carwash.php" class="btn btn-sm animated-button gibson-three">carwash</a> </div>
    <div class="col-md-3 col-sm-3 col-xs-6"> <a href="contact_photo.php" class="btn btn-sm animated-button gibson-four">contact</a> </div>
    <a href="gallery.php"><button>go back</button></a>
  </div>
  
  <footer>
<img src="img/facebook.png" width="40" height="40">
<img src="img/google.png" width="40" height="40">
<img src="img/instagram.png" width="40" height="40">
<img src="img/linkedin.png" width="40" height="40">
<img src="img/twitter.png" width="40" height="40">
<img src="img/youtube.png" width="40" height="40">
<h6>Powered by www.mirumagency.com | ©2017</h6>
</footer>
</body>
</html>