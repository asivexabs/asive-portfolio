<link rel="stylesheet" type="text/css" href="gallery.css">
<link rel="stylesheet" type="text/css" href="">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:200,400">
<link rel="stylesheet" type="text/css" href="nav.css">
<link rel="stylesheet" type="text/css" href="footer.css">
<header></header>
  <nav>
    <a href="#" id="menu__icon"></a>
  <ul>
    <a href="index.php"><li>Home</li></a>
    <a href="about.php"><li>About</li></a>
    <a href="gallery.php"><li>Gallery</li></a>
    <a href="contact.php"><li>Contact</li></a>
  </ul>
  </nav>
<section>

  <h1>Featured Work</h1>
  
<ul class="photo-stack-grid">
  <li>
    <a href="#d">
      <figure class="photo-stack">
        <img src="img/savanna-edge.png" /> 
      </figure>
      <span>Savanna-Edge</span>
    </a>
  </li>
  <li>
    <a href="https://codepen.io/sponge/pen/aJeyea">
      <figure class="photo-stack">
        <img src="img/mac d.png" />
      </figure>
      <span>Mac Donalds</span>
    </a>
  </li>
  <li>
    <a href="klipdrift.php">
      <figure class="photo-stack">
        <img src="img/klipdr.png" />
      </figure>
      <span>Klipdrift</span>
    </a>
  </li>
  <li>
    <a href="my-world.php">
      <figure class="photo-stack">
        <img src="img/gh.png" />
      </figure>
      <span>Sotope - Filtering & Sorting</span>
    </a>
  </li>
  <li>
    <a href="capitec.php">
      <figure class="photo-stack">
        <img src="img/capitec.png" />
      </figure>
      <span>Capitec</span>
    </a>
  </li>
  <li>
    <a href="disney.php">
      <figure class="photo-stack">
        <img src="img/positioon.png"/>
      </figure>
      <span>Disney</span>
    </a>
  </li>
  <li>
    <a href="https://codepen.io/sponge/pen/qrXvQR">
      <figure class="photo-stack">
        <img src="img/svg.png" />
      </figure>
      <span>SVG's</span>
    </a>
  </li>

  <li>
    <a href="https://codepen.io/sponge/pen/bWRyEx">
      <figure class="photo-stack">
        <img src="img/slick slides.png" />
      </figure>
      <span>Slick Slides</span>
    </a>
  </li>

</ul>

<a href="psd.php"><button>view photoshop psd</button></a>
<a href="explained.php"><button>explained projects</button></a>

</section>
<footer>
<img src="img/facebook.png" width="40" height="40">
<img src="img/google.png" width="40" height="40">
<img src="img/instagram.png" width="40" height="40">
<a href="https://www.linkedin.com/in/asive-xabendlini-734325137"><img src="img/linkedin.png" width="40" height="40"></a>
<img src="img/twitter.png" width="40" height="40">
<img src="img/youtube.png" width="40" height="40">
<h6>Powered by www.mirumagency.com | ©2017</h6>
</footer>
