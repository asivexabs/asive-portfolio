<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="nav.css">
    <link href="https://fonts.googleapis.com/css?family=Bad+Script" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Shadows+Into+Light|Montserrat:400,700">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:200,400">
    <link rel="stylesheet" type="text/css" href="footer.css">
    <!-- <link rel="stylesheet" type="text/css" href="web.css"> -->
    <link rel="stylesheet" type="text/css" href="about.css">
</head>
<body>
<header></header>
  <nav>
    <a href="#" id="menu__icon"></a>
  <ul>
    <a href="index.php"><li>Home</li></a>
    <a href="about.php"><li>About</li></a>
    <a href="gallery.php"><li>Gallery</li></a>
    <a href="contact.php"><li>Contact</li></a>
  </ul>
  </nav>
  <section>
<article> 
<h1>About Me</h1><br>

<p class="border1">I am driven motivated individual who always have the ego to learn new things mostly in areas where different people have to show and share different ideas and their expertise about different aspect of any given task or situation this has helped in developing my own intellectual capacity, exchanging of knowledge has helped myself in creating a mechanism to avoid all the aspect that will have a negative influence in my life. This strength is made possible and a success by the communication skills that I has learned in my community, Primary education, Secondary and in my tertiary education.</p><br>

<p class="border2">Peoples Skills is also one of my major qualities this quality is described and characterized by my positive attitude and discipline of understanding the individual differences because as people we do not face the same circumstances on daily basis there for it is imperative for me to accommodate each and every people’s situation at any given point in time. This skill is living within myself and the most characteristics that has made this skill to be unique within me is because I am open to experience talkative person always make sure that I pay more attention when person is speaking to me and I do also make sure that he get the answer or the information that is looking for.
</p><br>

<p class="border1">I am a leader because I can show guidance to my subordinate, the biggest quality when it comes to my leadership is my abstract reasoning, the ability to make deduction, I am critical thinker always I always think outside the box see sides where I believe other people cannot see however before I present something new to the other people I firstly  analyze within my and hear how is it going touch the other people and I also make predictions towards their response if ever  I introduce a specific topic.</p>
</article>
<aside>
<br>
<div class="pic"><img src="img/asive.jpg" alt="pic"></div><br>
<a href="personal.php"><button>Visit My Profile</button></a>
</aside>
</section><br>

  <footer>
<img src="img/facebook.png" width="40" height="40">
<img src="img/google.png" width="40" height="40">
<img src="img/instagram.png" width="40" height="40">
<img src="img/linkedin.png" width="40" height="40">
<img src="img/twitter.png" width="40" height="40">
<img src="img/youtube.png" width="40" height="40">
<h6>Powered by www.mirumagency.com | ©2017</h6>
</footer>
</body>
</html>