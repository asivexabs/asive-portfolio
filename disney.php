<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="nav.css">
    <link href="https://fonts.googleapis.com/css?family=Bad+Script" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Shadows+Into+Light|Montserrat:400,700">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:200,400">
    <link rel="stylesheet" type="text/css" href="footer.css">
    <!-- <link rel="stylesheet" type="text/css" href="web.css"> -->
    <link rel="stylesheet" type="text/css" href="capitec.css">
</head>
<body>
<header></header>
  <nav>
    <a href="#" id="menu__icon"></a>
  <ul>
    <a href="index.php"><li>Home</li></a>
    <a href="about.php"><li>About</li></a>
    <a href="gallery.php"><li>Gallery</li></a>
    <a href="contact.php"><li>Contact</li></a>
  </ul>
  </nav>
  <section>
<article> 
<img src="img/positioon.png">

<a href="explained.php"><button>go back</button></a>
</article>
<aside>
<h1>Disney Positioning</h1>
<p>Positioning is to put or arrange something in a particular place or way. In CSS we have position Absolute and position Relative. 
An element with position: absolute is positioned relative to the nearest positioned ancestor (instead of positioned relative to the viewport, like fixed). However if an absolute positioned element has no positioned ancestors, it uses the document body, and moves along with the page scrolling.
We used a deck of cards for inspiration to do the assignment, on the assignment we used these positioning to do the deck of cards and z-index property. We discovered that if you want to put something on top of another, the container must be position Relative and if the one on top you want to move it around it must be position Absolute.
</p>
<a href="https://codepen.io/sponge/pen/ryWQzJ"><button>codepen link</button></a>
</aside>
</section>
  <footer>
<img src="img/facebook.png" width="40" height="40">
<img src="img/google.png" width="40" height="40">
<img src="img/instagram.png" width="40" height="40">
<img src="img/linkedin.png" width="40" height="40">
<img src="img/twitter.png" width="40" height="40">
<img src="img/youtube.png" width="40" height="40">
<h6>Powered by www.mirumagency.com | ©2017</h6>
</footer>
</body>
</html>