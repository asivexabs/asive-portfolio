<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/header.css">
	<link rel="stylesheet" type="text/css" href="css/education.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
</head>
<body>
<header><!-- <h1>Header</h1> --></header>
  <nav>
    <a href="#" id="menu__icon"></a>
    <ul>
      <li><a href="index.php">Home</a></li>
      <li><a href="about.php">About</a></li>
      <li><a href="gallery.php">Gallery</a></li>
      <li><a href="contact.php">Contact</a></li>
    </ul>
  </nav>
  <br>
  <ul class="top-ul">
   <!--  <li>56-827 Thubelitsha Street</li>
    <li>Makhaza</li>
    <li>Khayelitsha</li>
    <li>7784</li> -->
    <li>asivex874@gmail.com</li>
    <li>083 671 6535</li>
  </ul>
<br>
<section><br>
<h1 class="h1">EDUCATIONAL QUALIFICATION</h1><br>
<h1 class="h2">SECONDARY EDUCATION</h1><br>
<article class="article">
  <ul class="middle-ul">
    <li>Institution</li>
    <li>Qualification Obtained</li>
    <li>Major Subjects</li><br><br>
    <li>Year Obtained</li>
  </ul>
</article> 
<aside class="aside">
  <ul class="large-ul">
    <li>: St Matthews High School</li>
    <li>: Grade 12, Matric Certificate</li>
    <li>: IsiXhosa, English, Physical Science,</li> 
    <li class="li">Mathematics, Life Orientation, Life Sciences</li> 
    <li class="li">and Geography</li>
    <li>: 2009</li>
   </ul><br>
</aside> 
<br><br>
<h1 class="h2">TERTIARY EDUCATION</h1><br>
<article class="article">
  <ul class="middle-ul">
    <li>Institution</li>
    <li>Qualification Obtained</li><br>
    <li>Major Subjects</li><br><br><br>
    <li>Year Obtained</li>
  </ul>
</article> 
<aside class="aside">
  <ul class="large-ul">
    <li>: College of Cape Town</li>
    <li>: National Certificate in Electrical Infrastructure </li>
    <li class="li">and Construction</li>
    <li>: Electrical Principle and Practice, Electrical</li> 
    <li class="li">System and Construction, Electronic Control and Digital</li> 
    <li class="li">Electronics, Workshop Practice, English first additional language,</li> 
    <li class="li"> Mathematics and Life Orientation.</li> 
    <li>: 2014</li>
   </ul><br>
</aside> <br>
<br><br><br>
<article class="article">
  <ul class="middle-ul">
    <li>Institution</li>
    <li>Qualification Obtained</li><br>
    <li>Major Subjects</li><br>
    <li>Year Obtained</li>
  </ul>
</article> 
<aside class="aside">
  <ul class="large-ul">
    <li>: Red and Yellow School</li>
    <li>: National Certificate in IT(System Development)</li>
    <li class="li">Level 5</li>
    <li>: Electrical Principle and Practice, Electrical</li> 
    <li class="li">System and </li>  
    <li>: 2017</li>
   </ul><br>
</aside> <br>
</section>
<a href="personal.php"><button class="button"><<< Previous Page</button></a><br>
<a href="work-experience.php"><button>Work Experience >>></button></a><br>
<?php
include ('footer.php');
?>