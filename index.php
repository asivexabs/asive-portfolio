<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="nav.css">
        
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Shadows+Into+Light|Montserrat:400,700">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:200,400">
    <link rel="stylesheet" type="text/css" href="footer.css">
    <link rel="stylesheet" type="text/css" href="web.css">
</head>
<body>
<header></header>
  <nav>
    <a href="#" id="menu__icon"></a>
  <ul>
    <a href="index.php"><li>Home</li></a>
    <a href="about.php"><li>About</li></a>
    <a href="gallery.php"><li>Gallery</li></a>
    <a href="contact.php"><li>Contact</li></a>
  </ul>
  </nav>
  <section>
      <h1>Portfolio Website</h1>
      <p> of Asive Xabendlini</p>
  </section>
  <footer>
<img src="img/facebook.png" width="40" height="40">
<img src="img/google.png" width="40" height="40">
<img src="img/instagram.png" width="40" height="40">
<img src="img/linkedin.png" width="40" height="40">
<img src="img/twitter.png" width="40" height="40">
<img src="img/youtube.png" width="40" height="40">
<h6>Powered by www.mirumagency.com | ©2017</h6>
</footer>
</body>
</html>