<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="nav.css">
    <link href="https://fonts.googleapis.com/css?family=Bad+Script" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Shadows+Into+Light|Montserrat:400,700">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:200,400">
    <link rel="stylesheet" type="text/css" href="footer.css">
    <!-- <link rel="stylesheet" type="text/css" href="web.css"> -->
    <link rel="stylesheet" type="text/css" href="capitec.css">
</head>
<body>
<header></header>
  <nav>
    <a href="#" id="menu__icon"></a>
  <ul>
    <a href="index.php"><li>Home</li></a>
    <a href="about.php"><li>About</li></a>
    <a href="gallery.php"><li>Gallery</li></a>
    <a href="contact.php"><li>Contact</li></a>
  </ul>
  </nav>
  <section>
<article> 
<img src="img/gh.png">

<a href="explained.php"><button>go back</button></a>
</article>
<aside>
<h1>Isotope - Filtering & Sorting</h1>
<p>Responsive web design is about using CSS and html to resize, hide, shrink enlarge or move the content to make it look good on any screen using the plugins and media query. Plugins is a software component that adds a specific feature to an existing computer program. When a program supports plug-ins, it enables customization. </p>
<p>A content delivery network (CDN) is a system of distributed servers (network) that deliver pages and other Web content to a user, based on the geographic locations of the user, the origin of the webpage and the content delivery server</p>
<a href="https://codepen.io/sponge/pen/dWzbxe"><button>codepen link</button></a>
</aside>
</section>
  <footer>
<img src="img/facebook.png" width="40" height="40">
<img src="img/google.png" width="40" height="40">
<img src="img/instagram.png" width="40" height="40">
<img src="img/linkedin.png" width="40" height="40">
<img src="img/twitter.png" width="40" height="40">
<img src="img/youtube.png" width="40" height="40">
<h6>Powered by www.mirumagency.com | ©2017</h6>
</footer>
</body>
</html>